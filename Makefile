SHELL = /bin/bash
.SHELLFLAGS := -eu -o pipefail -c # Bash strict mode is a blessing
.ONESHELL:

grep-logs:
	cd logs-latest
	echo "build errors"
	grep "BUILD-FAILED-ERROR" *-build.log|wc -l
	grep "BUILD-FAILED-ERROR" *-build.log
	echo "clone errors"
	grep "CLONE-FAILED-ERROR" *-clone.log|wc -l
	grep "CLONE-FAILED-ERROR" *-clone.log

clean-builds:
	rm -rf build---*

build:
	time bb rotdetector2.bb 
