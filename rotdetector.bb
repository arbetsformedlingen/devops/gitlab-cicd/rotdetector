(require '[babashka.process :as p]
         '[clojure.edn :as edn]
         '[babashka.fs :as fs])
(require '[babashka.cli :as cli])
(require '[clojure.string :as str])
(require '[taoensso.timbre :as timbre])
(import 'java.time.format.DateTimeFormatter
        'java.time.LocalDateTime)

(def formatter (DateTimeFormatter/ofPattern "yyyy-MM-dd-HH-mm-ss"))


;; * url encode decode

(defn urldecode [s]
  (java.net.URLDecoder/decode s "UTF-8"))

(defn urlencode [s]
  (java.net.URLEncoder/encode s "UTF-8"))

(defn encode-base26
  [num]
  (let [alphabet (map char (range 97 123))
        base (count alphabet)]
    (loop [n num
           res []]
      (if (zero? n)
        (apply str (reverse res))
        (let [remainder (mod n base)
              quotient (quot n base)]
          (recur quotient (cons (nth alphabet remainder) res)))))))

(defn encode-string-base26
  [instr]
  (let [bytes (.getBytes instr "UTF-8")
        encoded-bytes (map encode-base26 bytes)]
    (apply str encoded-bytes)))

(defn repo-url-to-build-dir [repo-url]
  (str "build---"
       (-> repo-url
           (str/replace "https://gitlab.com/arbetsformedlingen/" "")
           (str/replace "/" "---")
           (urlencode)))
  )
(defn repo-url-to-tag [repo-url]
  ;;(->> repo-url   .getBytes (.encodeToString (java.util.Base64/getEncoder)) ) ;; base64 wasnt good for this
  ;; base26 is ineficient mem wise, so trim the str a bit, so we dont break docker tag max len
  (encode-string-base26 (repo-url-to-build-dir repo-url))
  )

;; * building


;; clone a repo
(defn clone-repo [repo-url]
  (timbre/info "cloning " repo-url)
  (fs/create-dirs (repo-url-to-build-dir repo-url))
  (p/shell {:out *out* :err *out*} "git clone " repo-url (repo-url-to-build-dir repo-url)  )
  )

;; build a repo
(defn build-repo [repo-url]
  (timbre/info "build-repo " repo-url)
  (try 
    (p/shell {   :out *out* :err *out*
              :dir (repo-url-to-build-dir repo-url)}
             "podman build . --no-cache --tag " (repo-url-to-tag repo-url) )
    (catch Exception e
      ;;(timbre/error "build-repo: error building " e)
      (throw e)
      )
    )
  )

(defn next-element [lst el]
  (let [remain (next (drop-while #(not= % el) lst))]
    (first remain)))

(defn get-first-repo []
  (first (edn/read-string (slurp "repolist.edn"))))

(defn get-next-repo [repo-url]
  "return the next repo after repo-url, if at end, begin again from the start"
  (or (next-element (edn/read-string (slurp "repolist.edn")) repo-url)
      (get-first-repo))
  )

(defn get-last-repo []
  (edn/read-string (slurp "last-repo.edn")))

(defn write-last-repo-url [repo-url]
  (timbre/info "write last repo url " repo-url)
  (spit "last-repo.edn" (prn-str repo-url)))

(defn rmi [repo-url]
  (try 
    (timbre/info "deleting image " (repo-url-to-tag repo-url))
    (p/shell {:out *out* :err *out*} "podman rmi -f " (repo-url-to-tag repo-url) )
    (catch Exception e
      (throw e)
      )
    )
  )

(defn rm-build-dir [repo-url]
  (timbre/info "deleting build directory " (repo-url-to-build-dir repo-url))
  (fs/delete-tree (repo-url-to-build-dir repo-url))
  )

;;where to place logs for this run
(def logdir (str "logs" "-" (.format (LocalDateTime/now) formatter) "/") )
(fs/create-dirs logdir)


(defn clone-and-build-repo [repo-url]
  (let [writer (clojure.java.io/writer (str logdir "/" (repo-url-to-build-dir repo-url)  ".log" ))]
       (with-open [out-writer writer]
         (binding [*out*  out-writer
                   timbre/*config* (assoc-in timbre/*config*
                                             [:appenders :println]
                                             (timbre/println-appender {:stream out-writer}))]
           (timbre/info "starting clone-and-build-repo " repo-url)
           (try 
             (clone-repo repo-url)
             (build-repo repo-url)
             (rmi repo-url)
             (rm-build-dir repo-url)
             (catch Exception e
               (timbre/error e "BUILD-FAILED-ERROR")))
           (timbre/info "finished clone-and-build-repo " repo-url)
           )))
  )


(defn build-next-repo []
  
  (let [last-repo-url (get-last-repo)
        repo-url  (get-next-repo last-repo-url)
        ]
    (clone-and-build-repo repo-url)
    (write-last-repo-url repo-url)

    ))

;; * main

;;(future (build-next-repo))

(defn build-all-repos []
  (timbre/info "building all the repos")
  (let [repo-list (partition-all 10 (edn/read-string (slurp "repolist.edn")))]
    (doseq [repo-chunk repo-list]
      (timbre/info (count repo-chunk) "futures")
      (let [futures (doall (map #(future (clone-and-build-repo %)) repo-chunk))]
        (doall (map deref futures)))))
  (timbre/info "finished building all the repos")
)



(build-all-repos)

;;(clone-and-build-repo "https://gitlab.com/arbetsformedlingen/devops/script-container.git")

