(require '[babashka.process :as p]
         '[clojure.edn :as edn]
         '[babashka.fs :as fs])
(require '[babashka.cli :as cli])
(require '[clojure.string :as str])
(require '[taoensso.timbre :as timbre])
(import 'java.time.format.DateTimeFormatter
        'java.time.LocalDateTime)

(defn parse-config [filename]
  (edn/read-string  (slurp filename)))

(def config (parse-config "config.edn"))

(def images (keys (edn/read-string (slurp "prod-uniq-images.edn"))))

(defn repo-search-image [image]
  (let [repo (-> image
                 (str/split #"/")
                 last)]
    (-> (p/sh {:extra-env {"GITLAB_TOKEN" (:GITLAB_TOKEN config)}} "glab repo search -s " repo) :out println)
    )
  
  )

(doall (map repo-search-image images))
;;(repo-search-image  "batfish/frontend/jobtech-taxonomy-editor")
;; try:
;; bb findrepos.bb |grep https|grep -v infra|grep  arbetsformedlingen
