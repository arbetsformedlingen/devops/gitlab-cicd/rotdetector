(require '[babashka.process :as p]
         '[clojure.edn :as edn]
         '[babashka.fs :as fs])
(require '[babashka.cli :as cli])
(require '[clojure.string :as str])
(require '[taoensso.timbre :as timbre])
(import 'java.time.format.DateTimeFormatter
        'java.time.LocalDateTime)
(require '[clojure.core.async :as a])

(def formatter (DateTimeFormatter/ofPattern "yyyy-MM-dd-HH-mm-ss"))
;; * config stuff
(defn parse-config [filename]
  (edn/read-string  (slurp filename)))
(def config (parse-config "settings.edn"))


;; * url encode decode

(defn urlencode [s]
  (java.net.URLEncoder/encode s "UTF-8"))

(defn encode-base26
  [num]
  (let [alphabet (map char (range 97 123))
        base (count alphabet)]
    (loop [n num
           res []]
      (if (zero? n)
        (apply str (reverse res))
        (let [remainder (mod n base)
              quotient (quot n base)]
          (recur quotient (cons (nth alphabet remainder) res)))))))

(defn encode-string-base26
  [instr]
  (let [bytes (.getBytes instr "UTF-8")
        encoded-bytes (map encode-base26 bytes)]
    (apply str encoded-bytes)))

(defn repo-url-to-build-dir [repo-url]
  (str "build---"
       (-> repo-url
           (str/replace "https://gitlab.com/arbetsformedlingen/" "")
           (str/replace "/" "---")
           (urlencode)))
  )
(defn repo-url-to-tag [repo-url]
  ;;(->> repo-url   .getBytes (.encodeToString (java.util.Base64/getEncoder)) ) ;; base64 wasnt good for this
  ;; base26 is ineficient mem wise, so trim the str a bit, so we dont break docker tag max len
  (encode-string-base26 (repo-url-to-build-dir repo-url))
  )

;; * building

(def ^:dynamic *real-run* true) ;;as opposed to dry run

;; clone a repo
(defn clone-repo [repo-url]
  (loop [i 0]
    (fs/create-dirs (repo-url-to-build-dir repo-url))
    (let [repo-token-url (str/replace repo-url "https://" (str "https://oauth2:" (:GITLAB_TOKEN config) "@") )]
      (timbre/info "cloning " repo-token-url)

      
      (if (not (try 
                 (p/shell {:out *out* :err *out*} "git clone "  repo-token-url (repo-url-to-build-dir repo-url)  )
                 (catch Exception e
                   ;; a gazillion things can aparently break git cloning
                   (timbre/error "cloning didnt go well " i " " repo-token-url)
                   (fs/delete-tree (repo-url-to-build-dir repo-url))
                   nil)))
        (if (< i 10)
          (recur (inc i))
          (timbre/error "giving up cloning after " i " times " repo-token-url)
          )))))

;; build a repo
(defn build-repo [repo-url]
  (timbre/info "build-repo " repo-url)
  (try 
    (p/shell {   :out *out* :err *out*
              :dir (repo-url-to-build-dir repo-url)}
             "podman build . --no-cache --tag " (repo-url-to-tag repo-url) )
    (catch Exception e
      ;;(timbre/error "build-repo: error building " e)
      (throw e))))

(defn next-element [lst el]
  (let [remain (next (drop-while #(not= % el) lst))]
    (first remain)))

(defn get-first-repo []
  (first (edn/read-string (slurp "repolist.edn"))))

(defn get-next-repo [repo-url]
  "return the next repo after repo-url, if at end, begin again from the start"
  (or (next-element (edn/read-string (slurp "repolist.edn")) repo-url)
      (get-first-repo))
  )

(defn get-last-repo []
  (edn/read-string (slurp "last-repo.edn")))

(defn write-last-repo-url [repo-url]
  (timbre/info "write last repo url " repo-url)
  (spit "last-repo.edn" (prn-str repo-url)))

;; * remove img
(defn rmi [repo-url]
  (try 
    (timbre/info "deleting image " (repo-url-to-tag repo-url))
    (p/shell {:out *out* :err *out*} "podman rmi -f " (repo-url-to-tag repo-url) )
    (catch Exception e
      (timbre/error "failed deleting image " (repo-url-to-tag repo-url))
      ;;really flaky but sometimes theres an error but the image was deleted anyway, so i guess its not a build problem then, so i mask the error in that case
      (try 
        (if (= 0 (:exit (p/shell {:out *out* :err *out* :continue true} "podman image inspect " (repo-url-to-tag repo-url) )))
          (do
            (timbre/error "image verified here but it should be gone " (repo-url-to-tag repo-url))
            ;;(throw e);; return code 0 means we found the image, which means we failed to delete it so its really an error, so throw
            ) 
          (timbre/error "failed deleting image but its mysteriously gone anyway " (repo-url-to-tag repo-url)))
        (catch Exception e
          (timbre/error e "podman inspect threw an error but it shouldnt. sigh.")
          )
        )
      
      )))

;; * build dir
(defn rm-build-dir [repo-url]
  (timbre/info "deleting build directory " (repo-url-to-build-dir repo-url))
  (fs/delete-tree (repo-url-to-build-dir repo-url))
  )

;; * where to place logs for this run
(def logdir (str "logs" "-" (.format (LocalDateTime/now) formatter) "/") )
(fs/create-dirs logdir)
(p/shell {:continue true} "rm logs-latest")
(p/shell "ln -s " logdir "logs-latest" )

;; * clone step
(defn clone-step-repo [repo-url]
  (let [writer (clojure.java.io/writer (str logdir "/" (repo-url-to-build-dir repo-url)  "-clone.log" ))]
       (with-open [out-writer writer]
         (binding [*out*  out-writer
                   timbre/*config* (assoc-in timbre/*config*
                                             [:appenders :println]
                                             (timbre/println-appender {:stream out-writer}))]
           (timbre/info "starting clone-step-repo " repo-url)
           (try
             (if *real-run* 
               (clone-repo repo-url)
               (timbre/info "dry run clone"))
             (catch Exception e
               (timbre/error e "CLONE-FAILED-ERROR")))
           (timbre/info "finished clone-step-repo " repo-url)
           repo-url))))

;; * build step
(defn build-step-repo [repo-url]
  (let [writer (clojure.java.io/writer (str logdir "/" (repo-url-to-build-dir repo-url)  "-build.log" ))]
       (with-open [out-writer writer]
         (binding [*out*  out-writer
                   timbre/*config* (assoc-in timbre/*config*
                                             [:appenders :println]
                                             (timbre/println-appender {:stream out-writer}))]
           (timbre/info "starting build-step-repo " repo-url)
           (timbre/info "repo dir " (repo-url-to-build-dir repo-url))
           (if *real-run* 
             (try 
               (build-repo repo-url)
               (rmi repo-url)
               (rm-build-dir repo-url)
               (catch Exception e
                 (timbre/error e "BUILD-FAILED-ERROR")))
             (timbre/info "dry run build"))
           (timbre/info "finished build-step-repo " repo-url)
           repo-url
           )))
  )


;; * main


(defn logstep[s d s2]
  (timbre/info (format "%-10s[%03d] %s" s d s2)))

;; clone and build, async
;; only run clones serially, because running clones in parallel lead to some kind of shadow ban from gitlab
;; this doesnt matter much because builds are much slower than clones
;; so i start a build as soon as a clone finishes, and the end build time is about the same
;; as if all clones and all builds run in parallell
(defn build-async []
  (let [clone-channel (a/chan 100)
        build-channel (a/chan)
        end-channel (a/chan)
        repolist-in (edn/read-string (slurp "repolist.edn"))
        ] 

    (timbre/info "starting clone loop")

    (defn build-go [repomap]
      (a/go (a/>! build-channel 
                  (a/go (build-step-repo (:content repomap)))))
      )
    
    (a/go-loop [repolist repolist-in
                i 0]    
      (if (empty? repolist)
        (do
          (timbre/info "clone input empty, marking end")
          (a/>! clone-channel {:type :end
                               :content "clone-end-marker"
                               :i 666}
                )
          (a/close! clone-channel))
        
        (do
          (logstep "cloning" i (first repolist))
          (a/>! clone-channel {:type :repo
                               :content (clone-step-repo (first repolist))
                               :i i})
          (recur (rest repolist) (inc i)))))
    
    (timbre/info "starting build loop")
    (a/go-loop []
      (when-let [repomap (a/<! clone-channel)]
        (if (= :end (:type repomap))
          (do
            (timbre/info "clone end marker found, stop waiting for build jobs")
            ;;(a/close! end-channel )
            ;;wait for all the channels to finish
            ;;(a/>! build-channel "build-end-marker")

            (a/go-loop [i 0]
              (timbre/info "waiting for build " i " total " (count repolist-in))
              ;; the build channel contains build streams.
              ;; here we wait for each stream to finish, by 1st getting the stream object, then wait for that stream to finish
              ;; therefore the 2 pull operations
              (timbre/info "finished " i " " (a/<! (a/<! build-channel)))
              (if (< i (dec (count repolist-in)))
                (recur (inc i))
                (do (timbre/info "finished waiting for builds")
                    ;; signal the end
                    (async/close! end-channel)))))
          (do
            (logstep "building" (:i repomap) (:content repomap))
            (build-go   repomap) 
            (recur)))
        ))

    ;;block here, waiting for end signal
    (while (a/<!! end-channel));;atm it wont finish...
    (timbre/info "done!")
    ))

(defn -main [args]
  (if (:dry-run args)
    (binding [*real-run* false]
      (timbre/info "dry run")
      (build-async))
    (build-async)))
                                      
(-main (cli/parse-opts *command-line-args* ))
